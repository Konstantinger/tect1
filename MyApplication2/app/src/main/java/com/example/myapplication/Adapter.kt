package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class Adapter: RecyclerView.Adapter<Adapter.VH>() {
    lateinit var listt: MutableList<Pair<String,String>>
    fun setList(list: MutableList<Pair<String,String>>){
        this.listt = list
    }
    class VH(itemView: View): RecyclerView.ViewHolder(itemView){
        val textView: TextView =  itemView.findViewById(R.id.textView2)
        val button: Button = itemView.findViewById(R.id.button)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.item,parent,false))
    }

    override fun getItemCount(): Int {
        return listt.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.textView.text = listt[position].first
        holder.button.text = listt[position].second
        holder.button.setOnClickListener{MainActivity3.doSmth(listt[position].first)}
    }
}