package com.example.solodkovgym

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_in.*
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T




class SignInActivity : AppCompatActivity() {

    lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        auth = FirebaseAuth.getInstance()

        val currentUser = auth.currentUser
        if (currentUser != null) {
            letsGo()
        }
        database = Firebase.database.reference
    }

    fun onSignInCl(v: View) {
        auth.signInAnonymously()
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    Toast.makeText(baseContext, "Authentication failed", Toast.LENGTH_SHORT).show()
                }
            }
    }

    fun updateUI(account: FirebaseUser?) {
        if (account != null) {
//            val user = User(password.text.toString(), getSharedPreferences("Solodkov Gym", 0).getString("height", "0")!!, getSharedPreferences("Solodkov Gym", 0).getString("weight", "0")!!)
//            database.child("users").child(login.text.toString()).setValue(user)
            if (!TextUtils.isEmpty(login.text) && !TextUtils.isEmpty(password.text)) {
                val one = login.text.toString()
                val two = password.text.toString()
                var bruh = ""
                database.child("users").child(one).child("password").addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(error: DatabaseError) {}

                    override fun onDataChange(snapshot: DataSnapshot) {
                        bruh = snapshot.value.toString()
                    }

                })

                if (bruh == two) {
                    Toast.makeText(this, "Krasava!", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Bruh, jopa", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "Проверьте поля!", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
        }
    }

    fun letsGo() {

    }
}
