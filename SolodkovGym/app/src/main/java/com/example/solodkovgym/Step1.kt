package com.example.solodkovgym

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.LinearLayout

class Step1 : AppCompatActivity() {

    lateinit var keep: LinearLayout
    lateinit var weight: LinearLayout
    lateinit var build: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_step1)

        keep = findViewById(R.id.keep)
        weight = findViewById(R.id.weight)
        build = findViewById(R.id.build)
    }

    fun onKeepingFitCl(v: View) {
        keep.setBackgroundResource(R.drawable.yellowcornersbuttons)
        some()
    }

    fun onWeightLossCl(v: View) {
        weight.setBackgroundResource(R.drawable.yellowcornersbuttons)
        some()
    }

    fun onBuildMuscleCl(v: View) {
        build.setBackgroundResource(R.drawable.yellowcornersbuttons)
        some()
    }

    fun bruh() {
        Handler().postDelayed(Runnable {
            keep.isClickable = true
            build.isClickable = true
            weight.isClickable = true
            keep.setBackgroundResource(R.drawable.whitecornersbuttons)
            build.setBackgroundResource(R.drawable.whitecornersbuttons)
            weight.setBackgroundResource(R.drawable.whitecornersbuttons)
        }, 200)
    }

    fun some() {
        weight.isClickable = false
        keep.isClickable = false
        build.isClickable = false
        Handler().postDelayed(Runnable {
            val intent = Intent(this, Step2::class.java)
            startActivity(intent)
            bruh()
        }, 300)
    }
}
