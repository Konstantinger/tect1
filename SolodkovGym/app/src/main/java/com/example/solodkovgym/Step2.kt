package com.example.solodkovgym

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView

class Step2 : AppCompatActivity() {

    lateinit var sh: SharedPreferences
    lateinit var ed: SharedPreferences.Editor

    lateinit var nextbutton: LinearLayout
    lateinit var female: LinearLayout
    lateinit var male: LinearLayout
    lateinit var next: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_step2)

        nextbutton = findViewById(R.id.nextbutton)
        female = findViewById(R.id.female)
        male = findViewById(R.id.male)
        next = findViewById(R.id.next)

        sh = getSharedPreferences("Solodkov Gym", 0)
        ed = sh.edit()

        nextbutton.setBackgroundResource(R.drawable.whitecornersbuttons)
        next.setTextColor(getResources().getColor(R.color.ic_launcher_logo_bruh_background))
        male.setBackgroundResource(R.drawable.whitecornersbuttons)
        female.setBackgroundResource(R.drawable.whitecornersbuttons)

        nextbutton.isClickable = false
    }

    fun onFemaleCl(v: View) {
        ed.putBoolean("isMale", false)

        female.setBackgroundResource(R.drawable.yellowcornersbuttons)
        male.setBackgroundResource(R.drawable.whitecornersbuttons)

        Yey()
    }

    fun onMaleCl(v: View) {
        ed.putBoolean("isMale", true)

        male.setBackgroundResource(R.drawable.yellowcornersbuttons)
        female.setBackgroundResource(R.drawable.whitecornersbuttons)

        Yey()
    }

    fun Yey() {
        nextbutton.isClickable = true
        nextbutton.setBackgroundResource(R.drawable.yellowcornersbuttons)
        next.setTextColor(Color.argb(255, 255, 255, 255))
    }

    fun onNextButtonCl(v: View) {
        ed.commit()
        val intent = Intent(this, Step3::class.java)
        startActivity(intent)
        finish()
    }
}
