package com.example.solodkovgym

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.ImageView

class Step3 : AppCompatActivity() {

    lateinit var hands: Button
    lateinit var spine: Button
    lateinit var torso: Button
    lateinit var legs: Button
    lateinit var image: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_step3)

        hands = findViewById(R.id.hands)
        spine = findViewById(R.id.spine)
        torso = findViewById(R.id.torso)
        legs = findViewById(R.id.legs)
        image = findViewById(R.id.imagebruh)

        some(true)

        if(getSharedPreferences("Solodkov Gym", 0).getBoolean("isMale", true)) {
            image.setImageResource(R.drawable.man)
        } else {
            image.setImageResource(R.drawable.women)
        }

        hands.setBackgroundResource(R.drawable.whitecornerslogo)
        spine.setBackgroundResource(R.drawable.whitecornerslogo)
        torso.setBackgroundResource(R.drawable.whitecornerslogo)
        legs.setBackgroundResource(R.drawable.whitecornerslogo)
    }

    fun onHandsCl(v: View) {
        hands.setBackgroundResource(R.drawable.yellowcornerslogo)
        letsGo()
    }

    fun onSpineCl (v: View) {
        spine.setBackgroundResource(R.drawable.yellowcornerslogo)
        letsGo()
    }

    fun onTorsoCl (v: View) {
        torso.setBackgroundResource(R.drawable.yellowcornerslogo)
        letsGo()
    }

    fun onLegsCl (v: View) {
        legs.setBackgroundResource(R.drawable.yellowcornerslogo)
        letsGo()
    }

    fun letsGo () {
        some(false)
        Handler().postDelayed(Runnable {
            val intent = Intent(this, Step4::class.java)
            startActivity(intent)
            finish()
        }, 300)
    }

    fun some(v: Boolean) {
        hands.isClickable = v
        spine.isClickable = v
        torso.isClickable = v
        legs.isClickable = v
    }
}
