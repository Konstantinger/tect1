package com.example.solodkovgym

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout

class Step4 : AppCompatActivity() {

    lateinit var newbie: LinearLayout
    lateinit var keepon: LinearLayout
    lateinit var advanced: LinearLayout
    lateinit var next: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_step4)

        newbie = findViewById(R.id.newbie)
        keepon = findViewById(R.id.keepon)
        advanced = findViewById(R.id.advanced)
        next = findViewById(R.id.newnextbutton)
    }

    fun onNewbieCl(v: View) {
        newbie.setBackgroundResource(R.drawable.yellowcornersbuttons)
        keepon.setBackgroundResource(R.drawable.whitecornersbuttons)
        advanced.setBackgroundResource(R.drawable.whitecornersbuttons)

        Yey()
    }

    fun onKeepOnCl(v: View) {
        keepon.setBackgroundResource(R.drawable.yellowcornersbuttons)
        newbie.setBackgroundResource(R.drawable.whitecornersbuttons)
        advanced.setBackgroundResource(R.drawable.whitecornersbuttons)

        Yey()
    }

    fun onAdvancedCl(v: View) {
        advanced.setBackgroundResource(R.drawable.yellowcornersbuttons)
        keepon.setBackgroundResource(R.drawable.whitecornersbuttons)
        newbie.setBackgroundResource(R.drawable.whitecornersbuttons)

        Yey()
    }

    fun Yey() {
        next.isClickable = true
        next.setBackgroundResource(R.drawable.yellowcornersbuttons)
    }

    fun onNextCl(v: View) {
        val intent = Intent(this, Step5::class.java)
        startActivity(intent)
        finish()
    }
}
