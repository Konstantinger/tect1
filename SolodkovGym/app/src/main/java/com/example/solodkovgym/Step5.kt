package com.example.solodkovgym

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_step1.*
import kotlinx.android.synthetic.main.activity_step5.*

class Step5 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_step5)
    }

    fun onSaveCl(v: View) {
        if(!TextUtils.isEmpty(editText2.text) && !TextUtils.isEmpty(editText3.text)) {
            val sh = getSharedPreferences("Solodkov Gym", 0)
            val ed = sh.edit()

            ed.putString("weight", editText2.text.toString())
            ed.putString("height", editText3.text.toString())
            ed.putBoolean("isStarted", true)
            ed.commit()

            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            Toast.makeText(this, "Проверьте поля!", Toast.LENGTH_SHORT).show()
        }
    }
}
