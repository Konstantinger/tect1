package com.example.solodkovgym

data class User (
    var password: String = "",
    var height: String = "",
    var weight: String = ""
)